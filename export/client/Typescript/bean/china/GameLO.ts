module china {
     export class GameLO {
     
         private name: string;

         public getName(): string {
             return this.name;
         }

         public setName(name: string): void {
             this.name = name;
         }
     
         private id: number;

         public getId(): number {
             return this.id;
         }

         public setId(id: number): void {
             this.id = id;
         }
     
         private type: number;

         public getType(): number {
             return this.type;
         }

         public setType(type: number): void {
             this.type = type;
         }
     
         private status: number;

         public getStatus(): number {
             return this.status;
         }

         public setStatus(status: number): void {
             this.status = status;
         }
     
     }
}
