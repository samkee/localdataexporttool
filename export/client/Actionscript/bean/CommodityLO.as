package tech.iball
{
	public class CommodityLO
	{
    
        private var rewardId: number;

        public function getRewardId(): number {
            return this.rewardId;
        }

        public function setRewardId(rewardId: number): void {
            this.rewardId = rewardId;
        }
    
        private var name: string;

        public function getName(): string {
            return this.name;
        }

        public function setName(name: string): void {
            this.name = name;
        }
    
        private var id: number;

        public function getId(): number {
            return this.id;
        }

        public function setId(id: number): void {
            this.id = id;
        }
    
        private var free: number;

        public function getFree(): number {
            return this.free;
        }

        public function setFree(free: number): void {
            this.free = free;
        }
    
	}
}