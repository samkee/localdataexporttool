package cookie;

import com.alibaba.fastjson.JSONObject;
import constants.TemplateType;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.ProjectUtil;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * 用一句话描述该文件作用
 *
 * @Author pengsamkee
 * created on 2016/11/14 23:30.
 */
public class ProjectCookie {

    private static final Logger logger = LoggerFactory.getLogger(ProjectCookie.class);

    public ProjectCookie() {
    }

    private String language;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    private String configDirectoryPath;

    public String getConfigDirectoryPath() {
        return configDirectoryPath;
    }

    public void setConfigDirectoryPath(String configDirectoryPath) {
        this.configDirectoryPath = configDirectoryPath;
    }

    private String clientExportTemplateName;

    public String getClientExportTemplateName() {
        return clientExportTemplateName;
    }

    public void setClientExportTemplateName(String clientExportTemplateName) {
        this.clientExportTemplateName = clientExportTemplateName;
    }

    private String serverExportTemplateName;

    public String getServerExportTemplateName() {
        return serverExportTemplateName;
    }

    public void setServerExportTemplateName(String serverExportTemplateName) {
        this.serverExportTemplateName = serverExportTemplateName;
    }

    private List<String> clientTypes;

    public List<String> getClientTypes() {
        return clientTypes;
    }

    public void setClientTypes(List<String> clientTypes) {
        this.clientTypes = clientTypes;
    }

    private List<String> serverTypes;

    public List<String> getServerTypes() {
        return serverTypes;
    }

    public void setServerTypes(List<String> serverTypes) {
        this.serverTypes = serverTypes;
    }

    private List<TemplateSetting> templateSettings;

    public List<TemplateSetting> getTemplateSettings() {
        return templateSettings;
    }

    public void setTemplateSettings(List<TemplateSetting> templateSettings) {
        this.templateSettings = templateSettings;
    }

    public static TemplateSetting getTemplateSetting(List<TemplateSetting> templateSettings, String templateName) {
        for (TemplateSetting templateSetting : templateSettings) {
            if (templateSetting.getName().equals(templateName)) {
                return templateSetting;
            }
        }
        return null;
    }

    public static ProjectCookie getProjectCookie() {
        File file = ProjectUtil.getCookieFile("ProjectCookie.json");
        ProjectCookie projectCookie = null;
        try {
            String projectCookieJson = FileUtils.readFileToString(file, "UTF-8");
            projectCookie = JSONObject.parseObject(projectCookieJson, ProjectCookie.class);
        } catch (IOException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return projectCookie;
    }

    public static List<String> getTypes(ProjectCookie projectCookie, String templateType) {
        return templateType == TemplateType.CLIENT ? projectCookie.getClientTypes() : projectCookie.getServerTypes();
    }

    public static String getExportType(ProjectCookie projectCookie, String templateType) {
        return templateType == TemplateType.CLIENT ? projectCookie.getClientExportTemplateName() : projectCookie.getServerExportTemplateName();
    }

    public static void setExportType(ProjectCookie projectCookie, String templateType, String templateName) {
        if (templateType == TemplateType.CLIENT) {
            projectCookie.setClientExportTemplateName(templateName);
        } else {
            projectCookie.setServerExportTemplateName(templateName);
        }
    }

}
