package cookie;

/**
 * 用一句话描述该文件作用
 *
 * @Author pengsamkee
 * created on 2016/11/15 21:17.
 */
public class TemplateSetting {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String extension;

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    private String exportBeanRootPackage;

    public String getExportBeanRootPackage() {
        return exportBeanRootPackage;
    }

    public void setExportBeanRootPackage(String exportBeanRootPackage) {
        this.exportBeanRootPackage = exportBeanRootPackage;
    }

    private String exportBeanDirectory;

    public String getExportBeanDirectory() {
        return exportBeanDirectory;
    }

    public void setExportBeanDirectory(String exportBeanDirectory) {
        this.exportBeanDirectory = exportBeanDirectory;
    }

    private String exportJsonDirectory;

    public String getExportJsonDirectory() {
        return exportJsonDirectory;
    }

    public void setExportJsonDirectory(String exportJsonDirectory) {
        this.exportJsonDirectory = exportJsonDirectory;
    }
}
