package poi;

/**
 * Created by user1005 on 2017/5/8.
 */
public enum RecordType {

    ID(1, "配置列表"),
    PROPERTY(2, "配置项");

    private int code;

    private String msg;

    RecordType(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

}
