package poi;

import java.util.List;
import java.util.Map;

/**
 * Created by user1005 on 2017/4/18.
 */
public class ConfigEntity {

    private String className;

    private List<Map<String, Object>> content;

    private int recordType;

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public List<Map<String, Object>> getContent() {
        return content;
    }

    public void setContent(List<Map<String, Object>> content) {
        this.content = content;
    }

    public int getRecordType() {
        return recordType;
    }

    public void setRecordType(int recordType) {
        this.recordType = recordType;
    }

}
