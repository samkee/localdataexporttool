package language;

import util.ResourceBundleUtil;

import java.util.Locale;

/**
 * 用一句话描述该文件作用
 *
 * @Author pengsamkee
 * created on 2016/11/16 22:07.
 */
public class Language {

    private static final String zh_CN = "zh_CN";

    private static final String zh_TW = "zh_TW";

    private static final String en_US = "en_US";

    public static Locale locale = Locale.SIMPLIFIED_CHINESE;

    public static String getTranslation(String key) {
        return ResourceBundleUtil.getString("language", key, locale);
    }

    public static String[] getTranslations(String key) {
        String translation = getTranslation(key);
        String[] translations = translation.split(",");
        return translations;
    }

    public static void updateLocale(String language, boolean forceDefault) {
        Locale updateLocale = Locale.getDefault();
        switch (language) {
            case zh_CN:
                updateLocale = Locale.SIMPLIFIED_CHINESE;
                break;
            case zh_TW:
                updateLocale = Locale.TRADITIONAL_CHINESE;
                break;
            case en_US:
                updateLocale = Locale.US;
                break;
        }
        if (forceDefault) {
            locale = updateLocale;
            Locale.setDefault(locale);
        }
    }
}
