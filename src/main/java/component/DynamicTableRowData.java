package component;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * 用一句话描述该文件作用
 *
 * @Author pengsamkee
 * created on 2016/11/22 0:23.
 */
public class DynamicTableRowData {

    private final LinkedHashMap<String, SimpleStringProperty> row = new LinkedHashMap();

    protected List<String> colIds = new ArrayList();

    public DynamicTableRowData(List colIds) {
        this.colIds.addAll(colIds);
    }

    private Callback<TableColumn.CellDataFeatures<DynamicTableRowData, String>, ObservableValue<String>>
            mapCellValueFactory = param -> {
        SimpleStringProperty simpleStringProperty = param.getValue().getCol(param.getTableColumn().getId());
        return simpleStringProperty;
    };

    public DynamicTableRowData addRow(String[] colDatas) {
        DynamicTableRowData dynamicTableRowData = new DynamicTableRowData(this.colIds);
        for (int i = 0; i < colDatas.length; i++) {
            dynamicTableRowData.row.put(colIds.get(i), new SimpleStringProperty(colDatas[i]));
        }
        return dynamicTableRowData;
    }

    public DynamicTableRowData updateRow(String... colDatas) {
        for (int i = 0; i < colDatas.length; i++) {
            SimpleStringProperty dynamicTableRowData = row.get(colIds.get(i));
            dynamicTableRowData.setValue(colDatas[i]);
        }
        return this;
    }

    public DynamicTableRowData putColData(String colId, String data) {
        row.put(colId, new SimpleStringProperty(data));
        return this;
    }

    public String getColData(int i) {
        String colId = colIds.get(i);
        return row.get(colId).getValue();
    }

    public String getColId(int i) {
        String colId = colIds.get(i);
        return colId;
    }

    public String getColData(String colId) {
        return row.get(colId).getValue();
    }

    public SimpleStringProperty getCol(String colId) {
        return row.get(colId);
    }

    public LinkedHashMap<String, SimpleStringProperty> getRow() {
        return row;
    }

    public Callback<TableColumn.CellDataFeatures<DynamicTableRowData, String>, ObservableValue<String>> getMapCellValueFactory() {
        return mapCellValueFactory;
    }

    public List<String> getColIds() {
        return colIds;
    }

}

