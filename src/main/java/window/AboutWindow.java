package window;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import language.Language;

/**
 * 用一句话描述该文件作用
 *
 * @Author pengsamkee
 * create at 2016/11/16 18:41
 */
public class AboutWindow {

    public void init(Stage ownnerStage) {
        Stage stage = new Stage();
        stage.initOwner(ownnerStage);
        stage.setTitle(Language.getTranslation("about.title"));
        stage.getIcons().add(new Image("app.logo.png"));
        stage.setWidth(640);
        stage.setHeight(300);
        stage.initModality(Modality.NONE);
        stage.setMaximized(false);
        stage.setResizable(false);
        stage.initStyle(StageStyle.UNDECORATED);

        GridPane grid = new GridPane();
        grid.setVgap(22);
        grid.setHgap(8);
        grid.setPadding(new Insets(0, 0, 0, 0));

        final Label nameLabel = new Label();
        nameLabel.setText("Welcome to Iball® LocalDataExportTool™®");

        VBox vBox = new VBox();
        vBox.setSpacing(10);
        vBox.setPadding(new Insets(0, 0, 0, 20));
        vBox.getChildren().add(nameLabel);

        grid.addRow(0, new ImageView(new Image("about.top.png")));
        grid.addRow(1, vBox);

        Scene scene = new Scene(new Group(), 800, 300);
        Group root = (Group) scene.getRoot();
        root.getChildren().add(grid);
        stage.setScene(scene);
        stage.focusedProperty().addListener(event -> {
            if (!stage.isFocused()) stage.close();
        });

        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2);
        stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 2);

        stage.showAndWait();
    }

}
