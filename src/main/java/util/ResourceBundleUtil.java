package util;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by pengsamkee on 2016/10/31.
 */
public class ResourceBundleUtil {

    public static int getInt(String baseName, String key) {
        ResourceBundle rb = ResourceBundle.getBundle(baseName);
        return Integer.parseInt(rb.getString(key));
    }

    public static long getLong(String baseName, String key) {
        ResourceBundle rb = ResourceBundle.getBundle(baseName);
        return Long.parseLong(rb.getString(key));
    }

    public static String getString(String baseName, String key, Locale locale) {
        ResourceBundle rb = ResourceBundle.getBundle(baseName, locale);
        return rb.getString(key);
    }

}
