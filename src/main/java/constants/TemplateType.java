package constants;

/**
 * 用一句话描述该文件作用
 *
 * @Author pengsamkee
 * create at 2016/11/15 18:12
 */
public interface TemplateType {
    String CLIENT = "client";
    String SERVER = "server";

    static Object[] getTemplateTypes() {
        Object[] typies = {CLIENT, SERVER};
        return typies;
    }
}
